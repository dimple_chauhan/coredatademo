//
//  DatePickerDemo.swift
//  CoreDataDemo
//
//  Created by Kapil Dhawan on 31/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class DatePickerDemo: UIViewController {

    @IBOutlet weak var tfDOB: UITextField!
    
    let datePick = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
         createDatePicker()
      }
    
func createDatePicker()
{
    datePick.datePickerMode = .date
    tfDOB.inputView = datePick
    let toolbar = UIToolbar()
    toolbar.sizeToFit()
    
    let donebutton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClicked))
    toolbar.setItems([donebutton], animated: true)
    tfDOB.inputAccessoryView = toolbar
    }
    @objc func doneClicked(){
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        tfDOB.text = formatter.string(from: datePick.date)
        
        self.view.endEditing(true)
    }
   }
