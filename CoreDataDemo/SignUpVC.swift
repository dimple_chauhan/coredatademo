//
//  SignUpVC.swift
//  CoreDataDemo
//
//  Created by Kapil Dhawan on 29/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import CoreData

class SignUpVC: UIViewController {

    @IBOutlet weak var tfRegName: UITextField!
    @IBOutlet weak var tfRegUsername: UITextField!
    @IBOutlet weak var tfRegPass: UITextField!
    @IBOutlet weak var tfRegConPass: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func btnSignUpClicked(_ sender: Any) {
        
            if (self.tfRegName.text!.isEmpty || self.tfRegUsername.text!.isEmpty || self.tfRegPass.text!.isEmpty || self.tfRegConPass.text!.isEmpty) {
             
                print("Fill all the Details!!")
            }
                
            else if (self.tfRegPass.text != self.tfRegConPass.text){
            
               print("Passwords in both the fields must match!!")
                
            }
                
            else
            {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                let context = appDelegate.persistentContainer.viewContext
                
                let newuser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
                
                newuser.setValue(self.tfRegName.text, forKey: "name")
                newuser.setValue(self.tfRegUsername.text, forKey: "username")
                newuser.setValue(self.tfRegPass.text, forKey: "password")
                newuser.setValue(self.tfRegConPass.text, forKey: "confirmpass")
                
                self.tfRegName.text = ""
                self.tfRegUsername.text = ""
                self.tfRegPass.text = ""
                self.tfRegConPass.text = ""
                do
                {
                    try context.save()
                    print("Registered  Sucessfully")
                    self.dismiss(animated: true, completion: nil)
                }
                catch
                {
                    
                    print("Failed Saving")
                }
         
               
       
              }
            }
            
}
    
    
    
 


