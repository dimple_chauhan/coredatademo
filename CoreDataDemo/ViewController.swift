//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Kapil Dhawan on 29/01/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var tfUsername: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    

    @IBOutlet weak var btnLogin: UIButton!
    
    
    @IBOutlet weak var btnSignup: UIButton!
  //  var editProfile: EditProfileVC?
    
       var user: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
    
      
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
      
      
  }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        if (self.tfUsername.text!.isEmpty || self.tfPassword.text!.isEmpty) {
            
            print("Fill all the Details!!")
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
            
            let predicate = NSPredicate(format: "username = %@ AND password = %@", tfUsername.text!, tfPassword.text!)
            
            request.predicate = predicate
            do
            {
               let result = try context.fetch(request) as NSArray
                
                if result.count>0
                {
                    let objectentity = result.firstObject as! UsersMO
                    
                    if objectentity.username == tfUsername.text! && objectentity.password == tfPassword.text!
                    {
                        print("Login Succesfully")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let data = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                         
                        let backItem = UIBarButtonItem()
                        backItem.title = "Logout"
                        navigationItem.backBarButtonItem = backItem
                        data.username = tfUsername.text!
                       // editProfile?.passData(user: self.tfUsername.text!)
                        self.navigationController?.pushViewController(data, animated: true)
                       
                        self.tfUsername.text = ""
                         self.tfPassword.text = ""
                    }
                    else
                    {
                        print("Wrong username or password !!!")
                    }
                } else {
                    print("No such account exist!!")
                }
            }
                
            catch
            {
             
                print("error")
            }

    }
    }
    
    @IBAction func btnSignUpClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let data = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
      
        self.navigationController?.present(data, animated: true)
 
    }
    
    func passData() -> String{
        return user
    }
    
  


}
