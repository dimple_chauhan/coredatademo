

import UIKit
protocol ImgCell {
    func deleteCell(indx: Int)
}

class GalleryViewCell: UICollectionViewCell{
    
    
    @IBOutlet weak var photos: UIImageView!
    @IBOutlet weak var lblPhotoLabel: UILabel!
    @IBOutlet weak var btnClose: UIButton!
   
    var delegate: ImgCell?
    var index: IndexPath?
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        delegate?.deleteCell(indx: (index?.item)!)
    }
    
}
