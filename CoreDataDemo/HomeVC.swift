

import UIKit

class HomeVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    
    var arrPhoto = [UIImage?]()
    var arrLabel = [String]()

    @IBOutlet weak var tfImgName: UITextField!
    
    @IBOutlet weak var btnChooseImg: UIButton!
    
    @IBOutlet weak var imgCollectionView: UICollectionView!
    
    
    @IBOutlet weak var btnEditProfile: UIButton!
    let imagePick = UIImagePickerController()
    
    var username = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imgCollectionView.delegate = self
        imgCollectionView.dataSource = self
        
    
       
        let layout = imgCollectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        
        layout.minimumInteritemSpacing = 5
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        imgCollectionView.addGestureRecognizer(longPressGesture)
        
    }
   
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
            
        case .began:
            guard let selectedIndexPath = imgCollectionView.indexPathForItem(at: gesture.location(in: imgCollectionView)) else {
                break
            }
            imgCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            imgCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            imgCollectionView.endInteractiveMovement()
        default:
            imgCollectionView.cancelInteractiveMovement()
        }
    }
    
   
    
    @IBAction func btnEdit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        data.user = username
        self.navigationController?.present(data, animated: true)
    }
    
    
    @IBAction func btnChooseImgClicked(_ sender: Any) {
            if self.tfImgName.text!.isEmpty{
                let alert = UIAlertController(title: "Invalid", message: "Textfield must not be empty!!", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
            } else {
                imagePick.delegate = self
           
                self.arrLabel.append(self.tfImgName.text!)
        
                self.btnChooseImg.isUserInteractionEnabled = true
        
                let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                    self.openCamera()
                }))
        
                alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                    self.openGallary()
                }))
        
                alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    }
    
    
    
             
       
    func openCamera()
    {
      if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePick.sourceType = UIImagePickerController.SourceType.camera
            imagePick.allowsEditing = true
            self.present(imagePick, animated: true, completion: nil)
        
        }
        else{
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePick.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePick.allowsEditing = true
        self.present(imagePick, animated: true, completion: nil)
       
        }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
           self.arrPhoto.append(image)
           self.imgCollectionView.reloadData()
        }
        dismiss(animated: true, completion: nil)
    }
 
  
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrLabel.count
    }
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let photoItem = arrPhoto.remove(at: sourceIndexPath.item)
        let labelItem = arrLabel.remove(at: sourceIndexPath.item)
        arrPhoto.insert(photoItem, at: destinationIndexPath.item)
        arrLabel.insert(labelItem, at: destinationIndexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryViewCell
        
        cell.photos.image = self.arrPhoto[indexPath.item]
        cell.lblPhotoLabel.text = self.arrLabel[indexPath.item]
        cell.index = indexPath
        cell.delegate = self
       return cell
    }
 
}
extension HomeVC: ImgCell{
    func deleteCell(indx: Int) {
        arrPhoto.remove(at: indx)
        arrLabel.remove(at: indx)
        self.imgCollectionView.reloadData()
    }
    

}



