//
//  EditProfileVC.swift
//  CoreDataDemo
//
//  Created by Kapil Dhawan on 01/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import CoreData

class EditProfileVC: UIViewController {

    @IBOutlet weak var tfNewName: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    var user = String()
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    

 
    @IBAction func btnEditClicked(_ sender: Any) {
        
        if (self.tfNewName.text!.isEmpty || self.tfNewPassword.text!.isEmpty || self.tfConfirmPassword.text!.isEmpty) {
            
            print("Fill all the Details!!")
        } else if (self.tfNewPassword.text != self.tfConfirmPassword.text){
            
            print("Passwords in both the fields must match!!")
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
            
            let predicate = NSPredicate(format: "username = %@", user)
            
            request.predicate = predicate
            do
            {
                let test = try context.fetch(request)
                let updateProfile = test[0] as! NSManagedObject
                updateProfile.setValue(self.tfNewName.text, forKey: "name")
                updateProfile.setValue(self.tfNewPassword.text, forKey: "password")
                updateProfile.setValue(self.tfConfirmPassword.text, forKey: "confirmpass")
                
                self.tfNewName.text = ""
                self.tfNewPassword.text = ""
                self.tfConfirmPassword.text = ""
                
                do
                {
                    try context.save()
                    print("Updated Sucessfully")
                    self.dismiss(animated: true, completion: nil)
                }
                catch
                {
                    print("Failed Saving")
                }
            }
            catch {
                print("error")
            }
        }
    }
    
    @IBAction func btnDeleteClicked(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        let predicate = NSPredicate(format: "username = %@", user)
        
        request.predicate = predicate
        do
        {
            let test = try context.fetch(request)
            let deletedObject = test[0] as! NSManagedObject
            context.delete(deletedObject)
            
            do
            {
                try context.save()
                print("Deleted Sucessfully")
                self.dismiss(animated: true, completion: nil)
            }
            catch
            {
                
                print("Failed")
            }
            }
        catch {
            print("error")
        }
    }
}
